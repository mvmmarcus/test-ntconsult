import React from "react";
import NewApplication from "./NewApplication/NewApplication";

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header"></header>
      <NewApplication />
    </div>
  );
};

export default App;
