/* eslint-disable no-useless-escape */
import * as Yup from "yup";

const InputNumberValidation = () =>
  Yup.object().shape({
    value: Yup.number(),
  });

export default InputNumberValidation;
