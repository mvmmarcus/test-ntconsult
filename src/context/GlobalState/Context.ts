import React from "react";

interface Option {
  value: string;
  label: string;
}

interface ApliedContract {
  id: number;
  title: string;
}

interface GlobalState {
  firstSelectedOption?: Option;
  secondSelectedOption?: Option;
  thirdSelectedOption?: Option;
  showAllFields?: boolean;
  apliedContracts: ApliedContract[];
  setApliedContracts: (apliedContracts: ApliedContract[]) => void;
  setFirstSelectedOption: (firstSelectedOption?: Option) => void;
  setSecondSelectedOption: (secondSelectedOption?: Option) => void;
  setThirdSelectedOption: (thirdSelectedOption?: Option) => void;
  setShowAllFields: (showAllFields?: boolean) => void;
}

const initialState: GlobalState = {
  firstSelectedOption: undefined,
  thirdSelectedOption: undefined,
  secondSelectedOption: undefined,
  apliedContracts: [],
  setApliedContracts: () => {},
  setFirstSelectedOption: () => {},
  setSecondSelectedOption: () => {},
  setThirdSelectedOption: () => {},
  setShowAllFields: () => {},
};

const GlobalStateContext = React.createContext<GlobalState>(initialState);

const GlobalStateConsumer = GlobalStateContext.Consumer;

export { GlobalStateContext, GlobalStateConsumer };

export type { GlobalState };
