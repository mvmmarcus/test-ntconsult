import React, { useState } from "react";
import { GlobalStateContext } from "./Context";

interface Props {
  children: React.ReactNode;
}

interface Option {
  value: string;
  label: string;
}

interface ApliedContract {
  id: number;
  title: string;
}

export const GlobalStateProvider: React.FC<Props> = ({ children }) => {
  const [firstSelectedOption, setFirstSelectedOption] = useState<
    Option | undefined
  >(undefined);
  const [secondSelectedOption, setSecondSelectedOption] = useState<
    Option | undefined
  >(undefined);
  const [thirdSelectedOption, setThirdSelectedOption] = useState<
    Option | undefined
  >(undefined);
  const [showAllFields, setShowAllFields] = useState<boolean | undefined>(
    false
  );
  const [apliedContracts, setApliedContracts] = useState<ApliedContract[]>([]);

  return (
    <GlobalStateContext.Provider
      value={{
        firstSelectedOption,
        secondSelectedOption,
        thirdSelectedOption,
        showAllFields,
        apliedContracts,
        setApliedContracts,
        setShowAllFields,
        setThirdSelectedOption,
        setSecondSelectedOption,
        setFirstSelectedOption,
      }}
    >
      {children}
    </GlobalStateContext.Provider>
  );
};
