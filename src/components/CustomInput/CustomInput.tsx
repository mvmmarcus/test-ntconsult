import React, { useState } from "react";
import { Formik, Form, FormikProps } from "formik";
import InputNumberValidation from "../../validations/InputValidationNumber";
import InputMask from "react-input-mask";

import "./styles.css";

const PERCENTAGE_TYPE = "percentage";
const DOLLAR_SIGN_TYPE = "dollar_sign";

type Props = {
  label: string;
  type: "percentage" | "dollar_sign";
};

interface FormInterface {
  value: number;
}

const handleSubmit = (values: FormInterface) =>
  console.log("submited: ", values);

const CustomInput: React.FC<Props> = ({ label, type }) => {
  const [inputValue, setInputValue] = useState<number>(0.0);

  return (
    <div className="input-container">
      <label htmlFor="text">{label}</label>

      <Formik
        initialValues={{
          value: 0,
        }}
        onSubmit={(values: FormInterface, actions) => {
          handleSubmit(values);
          setTimeout(() => {
            actions.setSubmitting(false);
          }, 500);
        }}
        validationSchema={InputNumberValidation}
      >
        {(props: FormikProps<FormInterface>) => {
          const { touched, errors, handleBlur, handleChange } = props;
          return (
            <Form className="input-group">
              <InputMask
                mask={type === DOLLAR_SIGN_TYPE ? "9.99" : "9.9"}
                style={{ textAlign: "right" }}
                value={
                  inputValue === 0
                    ? type === DOLLAR_SIGN_TYPE
                      ? inputValue?.toFixed(3)
                      : inputValue?.toFixed(2)
                    : inputValue
                }
                onChange={handleChange("value")}
                onBlur={handleBlur("value")}
                onChangeCapture={(tagert: any) =>
                  setInputValue(tagert.target?.value)
                }
              />

              {/* <input
                style={{ textAlign: "right" }}
                maxLength={3}
                value={values.value}
                onChange={handleChange("value")}
                onBlur={handleBlur("value")}
              /> */}
              {touched.value &&
                errors.value &&
                console.log("inputValidationError: ", errors.value)}

              {type === PERCENTAGE_TYPE && (
                <div className="percentage-box">%</div>
              )}
              {type === DOLLAR_SIGN_TYPE && (
                <div className="dollar-sign-box">$</div>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default CustomInput;
