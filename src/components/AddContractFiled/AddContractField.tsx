import React from "react";
import { BiPlusCircle } from "react-icons/bi";

import "./styles.css";

type Props = {
  addContract?: boolean;
  contractType: "buy-rate" | "profit-split" | "hidden";
  handleAddContractField: () => void;
};

const contractTypesObj = {
  "buy-rate": "Buy Rate",
  "profit-split": "Profit Split",
  hidden: "",
};

const AddContractField: React.FC<Props> = ({
  addContract,
  contractType,
  handleAddContractField,
}) => {
  return addContract !== undefined ? (
    <div className="add-field">
      {addContract !== undefined ? <div className="add-field-arrow" /> : null}
      {addContract ? (
        <span>Split With Agent / Affiliate</span>
      ) : addContract !== undefined ? (
        <div>
          <BiPlusCircle
            onClick={handleAddContractField}
            className="add-field-button"
            size={20}
          />
          <p>
            Optional: Add {contractTypesObj[contractType]} with the Agent /
            Affiliate above
          </p>
        </div>
      ) : null}
    </div>
  ) : null;
};

export default AddContractField;
