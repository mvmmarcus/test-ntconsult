import React from "react";
import { BsTrash } from "react-icons/bs";
import editIcon from "../../img/edit.svg";

import "./styles.css";

type Props = {
  title: string;
};

const ApliedSmartContract: React.FC<Props> = ({ title }) => {
  return (
    <div className="aplied-smart-contract-container">
      <span>{title} #1</span>
      <div>
        <img src={editIcon} alt="edit-icon" />
        <BsTrash size={16} />
      </div>
    </div>
  );
};

export default ApliedSmartContract;
