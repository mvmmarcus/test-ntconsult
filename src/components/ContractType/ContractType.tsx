import React, { useContext, useEffect, useState } from "react";
import { GlobalStateContext } from "../../context";
import alertIcon from "../../img/alert-icon.svg";
import FormGroupContent from "../FormGroupContent/FormGroupContent";

import "./styles.css";

type Props = {
  type: "buy-rate" | "profit-split" | "hidden";
};

const titleObj = {
  "buy-rate": "Buy Rate",
  "profit-split": "Profit Split",
  hidden: "",
};

interface State {
  showDropDown: boolean;
  addSecondField: boolean;
  addThirdField: boolean;
}

const PROFIT_SPLIT_TYPE = "profit-split";

const ContractType: React.FC<Props> = ({ type }) => {
  const [state, setState] = useState<State>({
    showDropDown: false,
    addSecondField: false,
    addThirdField: false,
  });

  const {
    firstSelectedOption,
    secondSelectedOption,
    thirdSelectedOption,
    showAllFields,
    setFirstSelectedOption,
    setSecondSelectedOption,
    setThirdSelectedOption,
  } = useContext(GlobalStateContext);

  useEffect(() => {
    if (showAllFields) {
      setState({
        showDropDown: state.showDropDown,
        addSecondField: true,
        addThirdField: true,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showAllFields]);

  return (
    <div className="contract-type-container">
      <div className="title">
        <img src={alertIcon} alt="alert-icon" />
        <p>
          Nice explanation about the {titleObj[type]} smart contract. Lorem
          ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec diam ac
          eros vulputate maximus nec et mi.
        </p>
      </div>
      <div className="contract-type-content">
        <div
          className={`${
            state.addSecondField && "contract-type-content-before"
          }`}
        >
          <div className="title">
            <div className="marker"></div>
            <span>{titleObj[type]} Contract #1</span>
          </div>
          <div
            className={`form-group ${
              type === PROFIT_SPLIT_TYPE && "form-group--profit-split"
            }`}
          >
            <FormGroupContent
              selectedOption={firstSelectedOption}
              setSelectedOption={setFirstSelectedOption}
              addContract={state.addSecondField}
              handleAddContractField={() =>
                setState({
                  showDropDown: state.showDropDown,
                  addSecondField: true,
                  addThirdField: state.addThirdField,
                })
              }
              type={type}
            />

            {state?.addSecondField && (
              <>
                <FormGroupContent
                  selectedOption={secondSelectedOption}
                  setSelectedOption={setSecondSelectedOption}
                  marginLeft="form-group-select--32"
                  addContract={state.addThirdField}
                  handleAddContractField={() =>
                    setState({
                      showDropDown: state.showDropDown,
                      addSecondField: state.addSecondField,
                      addThirdField: true,
                    })
                  }
                  type={type}
                  showTrashIcon
                />
              </>
            )}
            {state?.addThirdField && (
              <>
                <FormGroupContent
                  selectedOption={thirdSelectedOption}
                  setSelectedOption={setThirdSelectedOption}
                  marginLeft="form-group-select--64"
                  addContract={undefined}
                  handleAddContractField={() =>
                    setState({
                      showDropDown: state.showDropDown,
                      addSecondField: state.addSecondField,
                      addThirdField: state.addThirdField,
                    })
                  }
                  type={type}
                  showTrashIcon
                />
              </>
            )}
          </div>
        </div>
        <div className="checkox-container">
          <input type="checkbox" checked />
          <span>Save as a Template</span>
        </div>
      </div>
    </div>
  );
};

export default ContractType;
