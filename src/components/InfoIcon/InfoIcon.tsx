import React from "react";
import "./styles.css";

const InfoIcon: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <defs>
        <style className="a b"></style>
      </defs>
      <path className="a" d="M0,0H24V24H0Z" />
      <path
        className="b"
        d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm1,15H11V11h2Zm0-8H11V7h2Z"
      />
    </svg>
  );
};

export default InfoIcon;
