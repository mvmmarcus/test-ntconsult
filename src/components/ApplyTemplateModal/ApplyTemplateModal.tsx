import React from "react";
import ReactModal from "react-modal";
import Button from "../Button/Button";
import CustomSelect from "../CustomSelect/CustomSelect";
import closeIcon from "../../img/close-icon.svg";

import "./styles.css";

interface Option {
  value: string;
  label: string;
}

type Props = {
  modalIsOpen: boolean;
  showDropDown: boolean;
  selectedOption: Option | undefined;
  handleHideDropDown: () => void;
  handleShowDropDown: () => void;
  handleHideModal: () => void;
  setSelectedOption: () => void;
  handleApplyTemplate: () => void;
};

const ApplyTemplateModal: React.FC<Props> = ({
  modalIsOpen,
  showDropDown,
  selectedOption,
  setSelectedOption,
  handleHideDropDown,
  handleHideModal,
  handleShowDropDown,
  handleApplyTemplate,
}) => {
  return (
    <ReactModal
      isOpen={modalIsOpen}
      onRequestClose={handleHideModal}
      className="modal"
      overlayClassName="overlay"
    >
      <div>
        <div className="modal-title">
          <span>Select a Template</span>
          <img onClick={handleHideModal} src={closeIcon} alt="close-icon" />
        </div>
        <div className="modal-select">
          <CustomSelect
            showDropDown={showDropDown}
            setSelectedOption={setSelectedOption}
            handleHideDropDown={handleHideDropDown}
            handleShowDropDown={handleShowDropDown}
          />
        </div>
      </div>
      <div className="modal-foooter">
        <Button onClick={handleHideModal} className="default-button red-hover">
          Cancel
        </Button>
        <Button
          onClick={handleApplyTemplate}
          className={`${selectedOption?.value ? "bg-button" : "disabled"}`}
        >
          Apply
        </Button>
      </div>
    </ReactModal>
  );
};

export default ApplyTemplateModal;
