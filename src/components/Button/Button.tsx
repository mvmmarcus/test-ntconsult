import React from "react";
import "./styles.css";
import InfoIcon from "../InfoIcon/InfoIcon";
import ReactTooltip from "react-tooltip";

type Props = {
  children: any;
  className?: string;
  tooltipMessage?: string;
  tooltipId?: string;
  onClick?: (event?: any) => void;
};

const DASHED_BUTTON_CLASSNAME = "dashed-button";

const Button: React.FC<Props> = ({
  children,
  className,
  tooltipMessage,
  tooltipId,
  onClick,
}) => {
  return (
    <button onClick={onClick} className={className}>
      {children}
      {className === DASHED_BUTTON_CLASSNAME && (
        <>
          <div data-tip data-for={tooltipId}>
            <InfoIcon />
          </div>
          <ReactTooltip
            className="tooltip"
            place="right"
            id={tooltipId}
            type="success"
          >
            <span>{tooltipMessage}</span>
          </ReactTooltip>
        </>
      )}
    </button>
  );
};

export default Button;
