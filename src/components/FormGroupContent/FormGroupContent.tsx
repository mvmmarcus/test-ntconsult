import React, { useState } from "react";
import { BsTrash } from "react-icons/bs";
import AddContractField from "../AddContractFiled/AddContractField";
import CustomInput from "../CustomInput/CustomInput";
import CustomSelect from "../CustomSelect/CustomSelect";

import "./styles.css";

interface Option {
  value: string;
  label: string;
}

type Props = {
  type: "buy-rate" | "profit-split" | "hidden";
  showTrashIcon?: boolean;
  addContract?: boolean;
  marginLeft?: string;
  selectedOption?: Option;
  setSelectedOption: (selectedOption: Option) => void;
  handleAddContractField: () => void;
};

interface State {
  showDropDown: boolean;
}

const BUY_RATE_TYPE = "buy-rate";

const FormGroupContent: React.FC<Props> = ({
  type,
  showTrashIcon,
  addContract,
  marginLeft,
  selectedOption,
  setSelectedOption,
  handleAddContractField,
}) => {
  const [state, setState] = useState<State>({ showDropDown: false });

  return (
    <>
      <div className="form-group-content ">
        <div className={`form-group-select ${marginLeft}`}>
          <CustomSelect
            showFilterLabel
            selectedOption={selectedOption}
            setSelectedOption={setSelectedOption}
            showDropDown={state?.showDropDown}
            handleHideDropDown={() =>
              setState({
                showDropDown: false,
              })
            }
            handleShowDropDown={() =>
              setState({
                showDropDown: true,
              })
            }
          />
          <AddContractField
            contractType={type}
            addContract={addContract}
            handleAddContractField={handleAddContractField}
          />
        </div>

        <div
          className={`form-group-input ${
            addContract === undefined && "form-group-input--last"
          }`}
        >
          {showTrashIcon ? (
            <>
              <CustomInput type="percentage" label="Profit Split" />
              <div className="trash">
                <BsTrash size={16} />
              </div>
            </>
          ) : type === BUY_RATE_TYPE ? (
            <>
              <CustomInput type="percentage" label="Processing Fee" />
              <CustomInput type="dollar_sign" label="Transaction Fee" />
            </>
          ) : (
            <CustomInput type="percentage" label="Split % from ISO" />
          )}
        </div>
      </div>
    </>
  );
};

export default FormGroupContent;
