import React, { useContext } from "react";
import OutsideAlerter from "../../services/OutsideClickIdentifier/OutsideClickIdentifier";
import Select from "react-select";
import { BiSearch } from "react-icons/bi";

import "./styles.css";
import { GlobalStateContext } from "../../context";

interface Option {
  value: string;
  label: string;
}

type Props = {
  showDropDown: boolean;
  selectedOption?: Option;
  showFilterLabel?: boolean;
  setSelectedOption: (selectedOption: Option) => void;
  handleHideDropDown: () => void;
  handleShowDropDown: () => void;
};

const options = [
  { value: "Jonathan Joestar", label: "Jonathan Joestar" },
  { value: "Dio Brando", label: "Dio Brando" },
  { value: "Erina Pendleton", label: "Erina Pendleton" },
  { value: "Jonathan Joestar", label: "Jonathan Joestar" },
  { value: "Dio Brando", label: "Dio Brando" },
  { value: "Erina Pendleton", label: "Erina Pendleton" },
  { value: "Jonathan Joestar", label: "Jonathan Joestar" },
  { value: "Dio Brando", label: "Dio Brando" },
  { value: "Erina Pendleton", label: "Erina Pendleton" },
];

const templatesOptions = [
  { value: "Alabama Jenkins", label: "Alabama Jenkins" },
  { value: "Alaska Portland", label: "Alaska Portland" },
  { value: "Arizona Jenkins", label: "Arizona Jenkins" },
  { value: "Arkansas Jenkins", label: "Arkansas Jenkins" },
  { value: "Leeroy Jenkins", label: "Leeroy Jenkins" },
  { value: "Stevenson Parkings", label: "Stevenson Parkings" },
];

const CustomSelect: React.FC<Props> = ({
  showDropDown,
  selectedOption,
  showFilterLabel,
  setSelectedOption,
  handleHideDropDown,
  handleShowDropDown,
}) => {
  const {
    setFirstSelectedOption,
    setSecondSelectedOption,
    setThirdSelectedOption,
  } = useContext(GlobalStateContext);

  const onSelectChange = (target: Option) => {
    if (templatesOptions.indexOf(target) >= 0) {
      setFirstSelectedOption(options[0]);
      setSecondSelectedOption(options[1]);
      setThirdSelectedOption(options[2]);
    } else setSelectedOption(target);
  };

  return (
    <OutsideAlerter onClickOutside={handleHideDropDown}>
      <div className="select-input">
        <label htmlFor="text">
          {showFilterLabel ? "Agent / Affiliate Name" : "Template Name"}
        </label>
        <Select
          value={
            showFilterLabel
              ? selectedOption
              : { value: "Alaska Portland", label: "Alaska Portland" }
          }
          styles={{
            placeholder: (defaultStyles) => {
              return {
                ...defaultStyles,
                color: "#CCCCCC",
                cursor: "pointer",
              };
            },
            indicatorSeparator: () => ({ display: "none" }),
            container: () => ({
              cursor: "pointer",
            }),
            control: () => ({
              display: "flex",
              border: "1px solid #CCCCCC",
              fontSize: 14,
              outline: "none",
              borderRadius: "4px",
              height: "40px",
            }),
          }}
          openMenuOnClick={false}
          menuIsOpen={false}
          isSearchable={false}
          onFocus={handleShowDropDown}
          className="select"
          placeholder="Search for an Agent / Affiliate"
        />
        {showDropDown && (
          <div
            style={{ top: showFilterLabel ? "72px" : "88px" }}
            className="dropdown"
          >
            <div className="dropdown-content">
              {showFilterLabel && (
                <span className="title">Agents / Affiliate</span>
              )}
              <Select
                styles={{
                  placeholder: (defaultStyles) => {
                    return {
                      ...defaultStyles,
                      color: "#CCCCCC",
                      cursor: "pointer",
                    };
                  },
                  indicatorSeparator: () => ({ display: "none" }),
                  dropdownIndicator: () => ({ display: "none" }),
                  container: () => ({
                    cursor: "pointer",
                    position: "relative",
                  }),
                  option: (defaultProps) => ({
                    ...defaultProps,
                    padding: "10px",
                    color: "#777777",
                    fontWeight: "bold",
                    fontSize: "14px",
                    letterSpacing: "0.22px",
                  }),
                  menu: (defaultProps) => ({
                    ...defaultProps,
                    border: "none",
                    boxShadow: "none",
                  }),
                  control: () => ({
                    display: "flex",
                    border: "1px solid #CCCCCC",
                    fontSize: 14,
                    outline: "none",
                    borderRadius: "4px",
                    height: "40px",
                  }),
                  menuList: (defaultProps) => ({
                    ...defaultProps,
                    maxHeight: showFilterLabel ? "120px" : "170px",
                  }),
                }}
                theme={(theme) => ({
                  ...theme,
                  colors: {
                    ...theme.colors,
                    primary25: "#FEF9E2",
                    primary: "#FEF9E2",
                  },
                })}
                options={showFilterLabel ? options : templatesOptions}
                isSearchable
                className="search-select"
                placeholder={
                  showFilterLabel
                    ? "Search Load Blancer"
                    : "Search for a Template"
                }
                onMenuClose={handleHideDropDown}
                onChange={(target: any) => onSelectChange(target)}
              />
              {showFilterLabel ? (
                <BiSearch className="search-icon" size={20} />
              ) : null}
            </div>
          </div>
        )}
      </div>
    </OutsideAlerter>
  );
};

export default CustomSelect;
