import React, { useContext, useEffect, useState } from "react";
import Button from "../components/Button/Button";
import "./styles.css";
import InfoIcon from "../components/InfoIcon/InfoIcon";
import ReactTooltip from "react-tooltip";
import ApliedSmartContract from "../components/ApliedSmartContract/ApliedSmartContract";
import ContractType from "../components/ContractType/ContractType";
import { GlobalStateContext } from "../context";
import ApplyTemplateModal from "../components/ApplyTemplateModal/ApplyTemplateModal";

interface ApliedContract {
  id: number;
  title: string;
}

interface State {
  renderingContent: string;
  apliedContracts?: ApliedContract[];
  modalIsOpen?: boolean;
  showDropDown: boolean;
}

const CREATE_RENDERING = "create";
const HIDDEN_RENDERING = "hidden";

const NewApplication: React.FC = () => {
  const { apliedContracts, setApliedContracts } = useContext(
    GlobalStateContext
  );

  const [state, setState] = useState<State>({
    renderingContent: "create",
    apliedContracts,
    modalIsOpen: false,
    showDropDown: false,
  });

  const emptyApliedContracts = !state?.apliedContracts?.length;

  const {
    firstSelectedOption,
    setShowAllFields,
    setFirstSelectedOption,
  } = useContext(GlobalStateContext);

  const handleShowModal = () => {
    setState({
      renderingContent: state.renderingContent,
      apliedContracts: state.apliedContracts,
      modalIsOpen: true,
      showDropDown: true,
    });
  };

  const handleHideModal = () => {
    setState({
      renderingContent: state.renderingContent,
      apliedContracts: state.apliedContracts,
      modalIsOpen: false,
      showDropDown: state.showDropDown,
    });
  };

  const handleShowDropDown = () => {
    setState({
      renderingContent: state.renderingContent,
      apliedContracts: state.apliedContracts,
      modalIsOpen: state.modalIsOpen,
      showDropDown: true,
    });
  };

  const handleHideDropDown = () => {
    setState({
      renderingContent: state.renderingContent,
      apliedContracts: state.apliedContracts,
      modalIsOpen: state.modalIsOpen,
      showDropDown: false,
    });
  };

  const handleApplyTemplate = () => {
    setShowAllFields(true);
    handleHideModal();
  };

  useEffect(() => {
    console.log("apliedContracts: ", apliedContracts);
  }, [apliedContracts]);

  const handleApplyContracts = () => {
    setApliedContracts([
      { id: 1, title: "Buy Rate Contract" },
      { id: 2, title: "Profit Split Contract" },
    ]);

    setState({
      renderingContent: "create",
      showDropDown: state.showDropDown,
    });
  };

  return (
    <>
      <ApplyTemplateModal
        modalIsOpen={state.modalIsOpen as boolean}
        showDropDown={state.showDropDown}
        selectedOption={firstSelectedOption}
        setSelectedOption={setFirstSelectedOption}
        handleHideModal={handleHideModal}
        handleHideDropDown={handleHideDropDown}
        handleShowDropDown={handleShowDropDown}
        handleApplyTemplate={handleApplyTemplate}
      />
      <section>
        <div className="section-title">
          <span className="section-title-description">
            Create a Smart Contract <span> (Optional)</span>
          </span>
          {state.renderingContent !== CREATE_RENDERING && (
            <Button onClick={handleShowModal} className="borded-button">
              Apply Template
            </Button>
          )}
        </div>

        {state.renderingContent === CREATE_RENDERING ? (
          <>
            <div className="contract-type">
              <div className="contract-type-title">
                <span>Select a contract type</span>

                <div data-tip data-for="explanation-buy-rate">
                  <InfoIcon />
                </div>

                <ReactTooltip
                  className="tooltip-explanation-buy-rate"
                  place="right"
                  id="explanation-buy-rate"
                  type="success"
                >
                  <span>Explanation on what is a Buy Rate Contract</span>
                </ReactTooltip>
              </div>

              <div className="contract-type-buttons">
                <Button
                  tooltipId="explanation-buy-rate-button"
                  tooltipMessage="Explanation on what is a Buy Rate Contract"
                  className="dashed-button"
                  onClick={() =>
                    setState({
                      renderingContent: "buy-rate",
                      showDropDown: state.showDropDown,
                    })
                  }
                >
                  Buy Rate
                </Button>
                <Button
                  tooltipId="explanation-profit-split-button"
                  tooltipMessage="Explanation about what is a Profit Split Contract"
                  className="dashed-button"
                  onClick={() =>
                    setState({
                      renderingContent: "profit-split",
                      showDropDown: state.showDropDown,
                    })
                  }
                >
                  Profit Split
                </Button>
              </div>
            </div>
            <div className="aplied-contracts">
              <div
                className={`aplied-contracts-title ${
                  emptyApliedContracts && "border-bottom"
                }`}
              >
                <span>Applied Smart Contracts</span>
              </div>
              <div
                className={`aplied-contracts-list ${
                  !emptyApliedContracts && "aplied-contracts-list--empty"
                }`}
              >
                {apliedContracts?.length ? (
                  apliedContracts.map(({ id, title }) => (
                    <ApliedSmartContract key={id} title={title} />
                  ))
                ) : (
                  <span>
                    You haven't created a smart contract yet. Start by selecting
                    a contract type above.
                  </span>
                )}
              </div>
            </div>
          </>
        ) : (
          <ContractType
            // @ts-ignore
            type={
              state.renderingContent !== CREATE_RENDERING
                ? state.renderingContent
                : HIDDEN_RENDERING
            }
          />
        )}

        <div className="footer">
          {state.renderingContent === CREATE_RENDERING ? (
            <>
              <Button className="default-button">Back</Button>
              <div>
                <Button className="borded-button">{"Save & Exit"}</Button>
                <Button className="bg-button">Skip and Submit</Button>
              </div>
            </>
          ) : (
            <>
              <Button
                onClick={() =>
                  setState({
                    renderingContent: "create",
                    showDropDown: state.showDropDown,
                  })
                }
                className="default-button"
              >
                Cancel
              </Button>
              <Button
                onClick={
                  firstSelectedOption?.value ? handleApplyContracts : () => {}
                }
                className={`${
                  firstSelectedOption?.value ? "bg-button" : "disabled"
                }`}
              >
                Add to Application
              </Button>
            </>
          )}
        </div>
      </section>
    </>
  );
};

export default NewApplication;
