# 🔗 Test Dev NTConsult

## Application of the selection process for React front-end developer at NT Consult

###

<div style="width: 100%; align-items: center; text-align: center;" >
<img align="center" alt="reactjs" src="https://img.shields.io/badge/v^16.9.53-react-blue"/>
<img align="center" alt="reactjs" src="https://img.shields.io/badge/v^4.0.3-typescript-blue"/>
<img align="center" alt="reactjs" src="https://img.shields.io/badge/v^3.6.4-sass-dc93b8"/>

</div>

![](demo-test-dev.gif)

###

### 🛠 Tecnologias

<div id="tecnologias" >
As seguintes ferramentas foram usadas no desenvolvimento da aplicação:

- [Reactjs](https://pt-br.reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Sass](https://sass-lang.com//)
</div>

###

### 🚀 Como executar o projeto

<div id="instalacao" >

### Clone este repositório

$ git clone git@gitlab.com:mvmmarcus/test-ntconsult.git

### Acesse a pasta do projeto no terminal/cmd

$ cd/test-ntconsult

### Instale as dependências

$ yarn ou npm install

### Execute a aplicação

$ yarn start ou npm run start

</div>

###

## 👨‍💻 Desenvolvedor

<table id="contribuicoes" >
  <tr>
    <td align="center"><a href="https://gitlab.com/mvmmarcus"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/6195744/avatar.png?width=400" width="100px;" alt=""/><br /><sub><b>Marcus Vinícius</b></sub></a><br /><a href="https://gitlab.com/mvmmarcus" title="Marcus Vinicius">👨‍🚀</a></td>
  </tr>
</table>

Desenvolvido com ❤️ por <a href="https://gitlab.com/mvmmarcus">Marcus Vinícius</a>
